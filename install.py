# Adapted from the IJava release:
# - IJava: https://github.com/SpencerPark/IJava
# - Ijava release: https://github.com/SpencerPark/IJava/releases/download/v1.3.0/ijava-1.3.0.zip

import argparse
import json
import os
import sys
import shutil

from jupyter_client.kernelspec import KernelSpecManager

JAR_PATH_ORIG = os.path.join(
    os.path.dirname(__file__),
    "target",
    "ikawa-0.1-SNAPSHOT-jar-with-dependencies.jar")


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Install the java kernel.')
    install_location = parser.add_mutually_exclusive_group()
    install_location.add_argument(
        '--user',
        help='Install to the per-user kernel registry.',
        action='store_true'
    )
    install_location.add_argument(
        '--sys-prefix',
        help="Install to Python's sys.prefix. Useful in conda/virtual environments.",
        action='store_true'
    )
    install_location.add_argument(
        '--prefix',
        help='''
        Specify a prefix to install to, e.g. an env.
        The kernelspec will be installed in PREFIX/share/jupyter/kernels/
        ''',
        default=''
    )
    return parser.parse_args()


def jupyter_install_jar(jar_path_orig: str,
                        arg_user,
                        arg_sys_prefix,
                        arg_prefix) -> str:
    # FIXME: types and default values of parameters

    # This dir will be copied by
    #   KernelSpecManager().install_kernel_spec
    tmp_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "kawa")
    jar_path_dest = os.path.join(tmp_dir, os.path.basename(jar_path_orig))
    try:
        os.mkdir(tmp_dir)
        # Put jar inside the dir that is going to be copied
        shutil.copyfile(jar_path_orig, jar_path_dest)
        install_dest = KernelSpecManager().install_kernel_spec(
            tmp_dir,
            kernel_name='kawa',
            user=arg_user,
            prefix=sys.prefix if arg_sys_prefix else arg_prefix
        )
        return install_dest
    finally:
        shutil.rmtree(tmp_dir)


def jupyter_install_kernel_json(install_dest: str,
                                jar_basename: str):
    # Prepare the token replacement string which should be properly escaped for use in a JSON string
    # The [1:-1] trims the first and last " json.dumps adds for strings.
    install_dest_escaped = json.dumps(install_dest)[1:-1]
    kernel_json_dict = {
        "argv": [
            "java",
            "-jar",
            os.path.join(install_dest_escaped, jar_basename),
            "{connection_file}"
        ],
        "display_name": "Kawa Scheme",
        "language": "kawa",
        "interrupt_mode": "message",
        "env": {}
    }
    kernel_json_dest = os.path.join(install_dest, 'kernel.json')
    with open(kernel_json_dest, 'w') as f:
        json.dump(kernel_json_dict, f, indent=4, sort_keys=True)


def main():
    args = parse_args()
    install_dest = jupyter_install_jar(JAR_PATH_ORIG,
                                       arg_user=args.user,
                                       arg_prefix=args.prefix,
                                       arg_sys_prefix=args.sys_prefix)
    jupyter_install_kernel_json(install_dest, os.path.basename(JAR_PATH_ORIG))


if __name__ == "__main__":
    main()
