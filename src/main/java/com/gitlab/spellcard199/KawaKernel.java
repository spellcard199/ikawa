/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 */

package com.gitlab.spellcard199;

import gnu.kawa.functions.Format;
import gnu.mapping.Environment;
import gnu.text.SourceMessages;
import io.github.spencerpark.jupyter.kernel.BaseKernel;
import io.github.spencerpark.jupyter.kernel.LanguageInfo;
import io.github.spencerpark.jupyter.kernel.ReplacementOptions;
import io.github.spencerpark.jupyter.kernel.display.DisplayData;
import kawa.standard.Scheme;
import kawadevutil.complete.find.CompletionFindGeneric;
import kawadevutil.complete.find.packagemembers.CompletionFindPackageMemberUtil;
import kawadevutil.complete.result.abstractdata.CompletionData;
import kawadevutil.complete.result.concretedata.CompletionForField;
import kawadevutil.complete.result.concretedata.CompletionForMethod;
import kawadevutil.complete.result.concretedata.CompletionForSymbolAndPackageMember;
import kawadevutil.eval.Eval;
import kawadevutil.eval.EvalResult;
import kawadevutil.eval.EvalResultAndOutput;
import kawadevutil.redirect.result.RedirectedOutErr;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class KawaKernel extends BaseKernel {

    private Logger glogger = java.util.logging.Logger.getGlobal();
    private Scheme scheme = new kawa.standard.Scheme();
    private kawadevutil.eval.Eval evaluator =
            new kawadevutil.eval.Eval(
                    // The base kernel we are using already takes care
                    // of capturing java.System.out.
                    Eval.SystemOutRedirectPolicy.NO_REDIRECT,
                    true,
                    true);

    private void log(Object obj) {
        glogger.log(Level.INFO, obj.toString());
    }

    public KawaKernel() {
        gnu.expr.Language.setCurrentLanguage(scheme);
        warmupCompletion();
    }

    private void warmupCompletion() {
        // Kawadevutil uses the ClassGraph library to get Info about Packages
        // and Classes, but the first time it takes a couple of seconds.
        // For this reason kawa-devutil keeps a cache of the default packages
        // and classes.
        // Here we are building the cache so that the first time the user asks
        // for completion the delay is not noticed.
        new Thread(() -> CompletionFindPackageMemberUtil.getRootCache(true)
        ).run();
    }

    @Override
    public DisplayData eval(String expr) throws Exception {
        EvalResultAndOutput evalResultPlusOutErr = this.evaluator
                .evalCatchingOutErr(scheme, scheme.getEnvironment(), expr);
        EvalResult evalResult = evalResultPlusOutErr.getResultOfSupplier();

        // The base kernel we are using already takes care of capturing
        // System.err and System.out when calling `eval'.

        SourceMessages messages = evalResult.getMessages();
        if (messages != null) {
            String messagesAsString = messages.toString(999999999);
            if (messagesAsString != null && !messagesAsString.isEmpty()) {
                System.err.println(messagesAsString);
            }
        }
        Throwable throwed = evalResult.getThrowed();
        if (throwed != null) {
            throwed.printStackTrace();
        }
        RedirectedOutErr outErr = evalResultPlusOutErr.getOutErr();
        if (outErr != null) {
            String err = evalResultPlusOutErr.getOutErr().getErr();
            if (err != null && !err.isEmpty()) {
                System.err.println(err);
            }
            String output = evalResultPlusOutErr.getOutErr().getOut();
            if (output != null && !output.isEmpty()) {
                System.out.println(output);
            }
        }

        Object result = evalResultPlusOutErr.getResultOfSupplier().getResult();
        String resultAsString;
        if (evalResult.isSuccess()) {
            if (result != null
                    && result.getClass().equals(gnu.mapping.Values.FromArray.class)) {
                return null;
            } else {
                return new DisplayData(Format.format("~A", result).toString());
            }
        } else {
            return null;
        }
    }

    @Override
    public LanguageInfo getLanguageInfo() {

        // TODO:
        // .version(engine.getFactory().getLanguageVersion())
        // .mimetype("text/javascript")
        // .codemirror("javascript")
        LanguageInfo.Builder lb
                = new LanguageInfo
                .Builder(scheme.getName())
                .version(kawa.Version.getVersion())
                .fileExtension(".scm")
                .pygments("Scheme");
        return lb.build();
    }

    @Override
    public ReplacementOptions complete(String code, int at) throws Exception {
        Optional<CompletionData> complDataMaybe = CompletionFindGeneric.find(
                code, at, this.scheme,
                Environment.user(), true
        );

        if (complDataMaybe.isPresent()) {

            CompletionData complData = complDataMaybe.get();
            String beforeCursor = null;

            if (complData.getCompletionType().equals(
                    CompletionData.CompletionType.METHODS)) {
                CompletionForMethod cm = complData.castTo().complForMethod();
                beforeCursor = cm.getCursorFinder().getCursorMatch().getBeforeCursor();

            } else if (complData.getCompletionType().equals(
                    CompletionData.CompletionType.FIELDS)) {
                CompletionForField cf = complData.castTo().complForField();
                beforeCursor = cf.getCursorFinder().getCursorMatch().getBeforeCursor();

            } else if (complData.getCompletionType().equals(
                    CompletionData.CompletionType.SYMBOLS_PLUS_PACKAGEMEMBERS)) {
                CompletionForSymbolAndPackageMember cspm
                        = complData.castTo().complForSymAndPackageMember();
                beforeCursor = cspm.getCommandCompleter().word;
            }

            if (beforeCursor != null) {
                // variable used in lambda must be effectively final.
                // Since we mutated beforeCursor it in the previous code,
                // we cant use it in filter.
                // That's why we are defining the new variable`prefix'.
                String prefix = beforeCursor;
                List<String> replacements = complData.getNames()
                        .stream()
                        .filter((String name) -> name.startsWith(prefix))
                        .distinct() // remove completions with same name but different signature
                        .collect(Collectors.toList());
                int sourceStart = at - beforeCursor.length();
                return new ReplacementOptions(replacements, sourceStart, at);
            }
        }
        return null;
    }
}
